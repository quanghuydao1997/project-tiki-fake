$(document).ready(function () {
    $(window).scroll(function () {
        if ($(window).scrollTop()) {
            $("header").addClass("sticky");
            $(".component__top").addClass("show");
            $(".header-icon").addClass("show");
            $("#header-icon-arrow").attr("src", "https://frontend.tikicdn.com/_mobile-next/static/img/icons/backBlue.svg");
            $("#header-icon-cart").attr("src", "https://frontend.tikicdn.com/_mobile-next/static/img/icons/cartBlue.svg");
            $("#header-icon-more").attr("src", "https://frontend.tikicdn.com/_mobile-next/static/img/icons/moreBlue.svg");
        } else {
            $("header").removeClass("sticky");
            $(".component__top").removeClass("show");
            $(".header-icon").removeClass("show");
            $("#header-icon-arrow").attr("src", "https://frontend.tikicdn.com/_mobile-next/static/img/icons/backWhite.svg");
            $("#header-icon-cart").attr("src", "https://frontend.tikicdn.com/_mobile-next/static/img/icons/cart.svg");
            $("#header-icon-more").attr("src", "https://frontend.tikicdn.com/_mobile-next/static/img/icons/moreWhite.svg");
        }
    });
});

var counter = 1;
setInterval(function () {
    document.getElementById('radio' + counter).checked = true;
    counter++;
    if (counter > 12) {
        counter = 1;
    }
}, 3000);


window.addEventListener("DOMContentLoaded", (e) => {
    render();
});

function render() {
    var removeCartItemButtons = document.getElementsByClassName('product__delete');
    for (let i = 0; i < removeCartItemButtons.length; i++) {
        const button = removeCartItemButtons[i];
        button.addEventListener('click', removeCartItem)
    }

    var quantityInputs = document.getElementsByClassName('qty-input');
    for (var i = 0; i < quantityInputs.length; i++) {
        var input = quantityInputs[i];
        input.addEventListener('change', quantityChanged)
    }

    var addToCartButtons = ducument.getElementsByClassName('qty-increase')
    for (var i = 0; i < addToCartButtons.length; i++) {
        var button = addToCartButtons[i];
        button.addEventListener
    }
}

function removeCartItem(e) {
    var buttonClicked = e.target;
    buttonClicked.parentElement.parentElement.parentElement.remove()
    updateCartTotal()
}

function quantityChanged(e) {
    var input = e.target;
    if (isNaN(input.value) || input.value <= 0) {
        input.value = 1
    }
    updateCartTotal()
}

function updateCartTotal() {
    var cartItemContainer = document.getElementsByClassName('cart-items')[0];
    var cartRows = cartItemContainer.getElementsByClassName('cart-row');
    var cartTotal = document.getElementsByClassName('prices__value');
    var total = 0;
    for (var i = 0; i < cartRows.length; i++) {
        var cartRow = cartRows[i];
        var priceElement = cartRow.getElementsByClassName('product__real-prices')[0];
        var quantityElement = cartRow.getElementsByClassName('qty-input')[0];

        var price = parseFloat(priceElement.innerText);
        var quantity = quantityElement.value;
        total = total + (price * quantity);
        console.log(total);
    }
    total = Math.round(total)
    cartTotal[0].innerText = total + ' đ';
    cartTotal[1].innerText = total + ' đ';
}


// lấy phần Modal
var modal = document.getElementById('myModal');

// Lấy phần button mở Modal
var btn = document.getElementById("myBtn");

// Lấy phần span đóng Modal
var span = document.getElementsByClassName("close")[0];

// Khi button được click thi mở Modal
btn.onclick = function () {
    modal.style.display = "block";
}

// Khi span được click thì đóng Modal
span.onclick = function () {
    modal.style.display = "none";
}

// Khi click ngoài Modal thì đóng Modal
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}